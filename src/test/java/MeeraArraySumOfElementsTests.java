import org.junit.Assert;
import org.junit.Test;


public class MeeraArraySumOfElementsTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = MeeraArraySumOfElements.isMeera( new int[] {-4, 0, 1, 0, 2, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 0;
		int actual = MeeraArraySumOfElements.isMeera( new int[] {-8, 0, 0, 8, 0} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass3() {
		
		int expected = 0;
		int actual = MeeraArraySumOfElements.isMeera( new int[]{-8, 0, 0, 2, 0} );
		Assert.assertEquals(expected, actual);

	}

}
