import org.junit.Assert;
import org.junit.Test;

public class TwinArrayTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = TwinArray.isTwin( new int[] {3, 5, 8, 10, 27} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = TwinArray.isTwin( new int[] {11, 9, 12, 13, 23} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = TwinArray.isTwin( new int[] {11, 9, 12, 13} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = TwinArray.isTwin( new int[] {5, 3, 14, 7, 18, 67} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 0;
		int actualValue = TwinArray.isTwin( new int[] {13, 14, 15, 3, 5} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass6() {
		int expectedValue = 0;
		int actualValue = TwinArray.isTwin( new int[] {1, 17, 8, 25, 67} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	

}
