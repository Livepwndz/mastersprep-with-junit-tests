import org.junit.Assert;
import org.junit.Test;



public class BeanArrayVersionTwoTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = BeanArrayVersionTwo.isBean( new int[] {2, 10, 9, 3} );
		Assert.assertEquals(expected, actual);

	}
	
	// {2, 2, 3, 3, 3}, {1, 1, 1, 2, 1, 1} and {0, -1, 1}.
	@Test
	public void shouldPass2() {
		int expected = 1;
		int actual = BeanArrayVersionTwo.isBean( new int[] {2, 2, 3, 3, 3} );
		Assert.assertEquals(expected, actual); 

	}
	
	@Test
	public void shouldPass3() {
		int expected = 1;
		int actual = BeanArrayVersionTwo.isBean( new int[] {1, 1, 1, 2, 1, 1} );
		Assert.assertEquals(expected, actual); 

	}
	
	@Test
	public void shouldPass4() {
		int expected = 1;
		int actual = BeanArrayVersionTwo.isBean( new int[] {0, -1, 1} );
		Assert.assertEquals(expected, actual); 

	}
	
	@Test
	public void shouldPass5() {
		int expected = 0;
		int actual = BeanArrayVersionTwo.isBean( new int[] {3, 4, 5, 7} );
		Assert.assertEquals(expected, actual); 

	}

}
