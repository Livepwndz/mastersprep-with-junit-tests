import org.junit.Assert;
import org.junit.Test;



public class HollowArrayTests {
	
	

	@Test
	public void shouldPass1() {
		int isHallowStatus = HollowArray.isHallow(new int[] { 8,1, 2, 0, 0,0,3, 4,1 });
		Assert.assertTrue( isHallowStatus == 1 );
	}
	
	@Test
	public void shouldPass2() {
		int isHallowStatus = HollowArray.isHallow(new int[] {1,2,4,0,0,0,3,4,5});
		Assert.assertTrue( isHallowStatus == 1 );
	}
	
	@Test
	public void shouldPass3() {
		int isHallowStatus = HollowArray.isHallow(new int[] {1,2,4,9, 0,0,0,3,4, 5});
		Assert.assertTrue( isHallowStatus == 0 );
	}
	
	@Test
	public void shouldPass4() {
		int isHallowStatus = HollowArray.isHallow(new int[] {1,2, 0,0, 3,4});
		Assert.assertTrue( isHallowStatus == 0 );
	}

	

}
