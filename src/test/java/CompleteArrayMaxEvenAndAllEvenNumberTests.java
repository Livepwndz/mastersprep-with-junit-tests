import org.junit.Assert;
import org.junit.Test;

public class CompleteArrayMaxEvenAndAllEvenNumberTests {

	@Test
	public void shouldPass1() {
		int expected = 1;
		int actual = CompleteArrayMaxEvenAndAllEvenNumber.isComplete( new int[] {2, 3, 2, 4, 11, 6, 10, 9, 8});
		Assert.assertEquals( expected, actual );
	}
	
	@Test
	public void shouldPass2() {
		int expected = 0;
		int actual = CompleteArrayMaxEvenAndAllEvenNumber.isComplete( new int[] {2, 3, 3, 6});
		Assert.assertEquals( expected, actual );
	}

}
