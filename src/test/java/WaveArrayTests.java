import org.junit.Assert;
import org.junit.Test;

public class WaveArrayTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = WaveArray.isWave( new int[] {7, 2, 9, 10, 5} );
		Assert.assertTrue( actualValue+"", expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = WaveArray.isWave( new int[] {4, 11, 12, 1, 6} );
		Assert.assertTrue( actualValue+"", expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = WaveArray.isWave( new int[] {1, 0, 5} );
		Assert.assertTrue( actualValue+"", expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = WaveArray.isWave( new int[] {2} );
		Assert.assertTrue( actualValue+"", expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 0;
		int actualValue = WaveArray.isWave( new int[] {2, 6, 3, 4} );
		Assert.assertTrue( actualValue+"", expectedValue == actualValue );
	}

}
