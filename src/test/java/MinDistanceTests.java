import org.junit.Assert;
import org.junit.Test;

public class MinDistanceTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = MinDistance.findFor(8);
		Assert.assertTrue( actualValue+"", actualValue == expectedValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 0;
		int actualValue = MinDistance.findFor(1);
		Assert.assertTrue( actualValue+"", actualValue == expectedValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 2;
		int actualValue = MinDistance.findFor(13013);
		Assert.assertTrue( actualValue+"", actualValue == expectedValue );
	}

}
