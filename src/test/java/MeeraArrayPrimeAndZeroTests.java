import org.junit.Assert;
import org.junit.Test;


public class MeeraArrayPrimeAndZeroTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = MeeraArrayPrimeAndZero.isMeera( new int[] {7, 6, 0, 10, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 1;
		int actual = MeeraArrayPrimeAndZero.isMeera( new int[] {6, 10, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass3() {
		
		int expected = 0;
		int actual = MeeraArrayPrimeAndZero.isMeera( new int[] {7, 6, 10} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass4() {
		
		int expected = 0;
		int actual = MeeraArrayPrimeAndZero.isMeera( new int[] {6, 10, 0} ); 
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass5() {
		int expected = 1;
		int actual = MeeraArrayPrimeAndZero.isMeera( new int[] {3, 7, 0, 8, 0, 5} );
		Assert.assertEquals(expected, actual);

	}
	


}
