import org.junit.Assert;
import org.junit.Test;


public class NiceArraySumAndFirstElementTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {21, 3, 7, 9, 11, 4, 6} );
		Assert.assertEquals(expectedValue, actualValue );

	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {13, 4, 4,4, 4} );
		Assert.assertEquals(expectedValue, actualValue );

	} //{10, 5, 5}, {0, 6, 8, 20} and {3}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {13, 4, 4,4, 4} );
		Assert.assertEquals(expectedValue, actualValue );

	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {10, 5, 5} );
		Assert.assertEquals(expectedValue, actualValue );

	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {0, 6, 8, 20} );
		Assert.assertEquals(expectedValue, actualValue );

	}
	
	@Test
	public void shouldPass6() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {3} );
		Assert.assertEquals(expectedValue, actualValue );

	} // {8, 5, -5, 5, 3}
	
	@Test
	public void shouldPass7() {
		int expectedValue = 1;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {0} );
		Assert.assertEquals(expectedValue, actualValue );

	}
	
	@Test
	public void shouldPass8() {
		int expectedValue = 0;
		int actualValue = NiceArraySumAndFirstElement.isNiceArray( new int[] {8, 5, -5, 5, 3} );
		Assert.assertEquals(expectedValue, actualValue );

	}

}
