import org.junit.Assert;
import org.junit.Test;


public class FibonacciNumberTests {

	//3, 5, 8, 13, 21,�.
	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 1 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 2 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass3() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 3 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass4() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 5 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass5() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 8 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass6() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 13 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass7() {
		
		int expected = 1;
		int actual = FibonacciNumber.isFibonacci( 21 );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass8() {
		
		int expected = 0;
		int actual = FibonacciNumber.isFibonacci( 4 );
		Assert.assertEquals(expected, actual);
	}


}
