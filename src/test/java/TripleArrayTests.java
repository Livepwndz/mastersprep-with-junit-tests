import org.junit.Assert;
import org.junit.Test;


public class TripleArrayTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = TripleArray.isTriple( new int[] {3, 1, 2, 1, 3, 1, 3, 2, 2} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 0;
		int actual = TripleArray.isTriple( new int[] {2, 5, 2, 5, 5, 2, 5} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass3() {
		
		int expected = 1;
		int actual = TripleArray.isTriple( new int[] {2, 5, 2, 5, 5, 2 } );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass4() {
		
		int expected = 0;
		int actual = TripleArray.isTriple( new int[] {3, 1, 1, 1} );
		Assert.assertEquals(expected, actual);

	}

}
