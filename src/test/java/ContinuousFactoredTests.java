import org.junit.Assert;
import org.junit.Test;

public class ContinuousFactoredTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = ContinuousFactored.isContinuousFactored( 6 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = ContinuousFactored.isContinuousFactored( 90 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 0;
		int actualValue = ContinuousFactored.isContinuousFactored( 121 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 0;
		int actualValue = ContinuousFactored.isContinuousFactored( 13 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	
	@Test
	public void shouldPass5() {
		int expectedValue = 0;
		int actualValue = ContinuousFactored.isContinuousFactored( 2 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass6() {
		int expectedValue = 0;
		int actualValue = ContinuousFactored.isContinuousFactored( 60 );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass7() {
		int expectedValue = 0;
		int actualValue = ContinuousFactored.isContinuousFactored( 120 );
		Assert.assertTrue( expectedValue == actualValue );

	}

}
