import org.junit.Assert;
import org.junit.Test;

public class DigitCountInNumberTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 2;
		int actualValue = DigitCountInNumber.countDigit(32121, 1);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = -1;
		int actualValue = DigitCountInNumber.countDigit(32121, -1);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = DigitCountInNumber.countDigit(1, 1);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = DigitCountInNumber.countDigit(0, 0);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 4;
		int actualValue = DigitCountInNumber.countDigit(33331, 3);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass6() {
		int expectedValue = 0;
		int actualValue = DigitCountInNumber.countDigit(33331, 6);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass7() {
		int expectedValue = 1;
		int actualValue = DigitCountInNumber.countDigit(3, 3);
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );
	}

}
