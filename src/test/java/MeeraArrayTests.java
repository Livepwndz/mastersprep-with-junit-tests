import org.junit.Assert;
import org.junit.Test;

public class MeeraArrayTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = MeeraArray.isMeera( new int[] {3, 5, -2} );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 0;
		int actualValue = MeeraArray.isMeera( new int[] {8, 3, 4} );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = MeeraArray.isMeera( new int[] {8} );
		Assert.assertTrue( expectedValue == actualValue );

	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 0;
		int actualValue = MeeraArray.isMeera( new int[] {8,16} );
		Assert.assertTrue( expectedValue == actualValue );

	}

}
