import org.junit.Assert;
import org.junit.Test;


public class SetEqualArrayTests {

	@Test
	public void shouldPass1() {
		
		int expectedValue = 1;
		int actualValue = SetEqualArray.isSetEqual( new int[] {}, new int[] {} );
		Assert.assertEquals( expectedValue, actualValue);

	}
	
	
	@Test
	public void shouldPass2() {
		
		int expectedValue = 1;
		int actualValue = SetEqualArray.isSetEqual( new int[] {1, 9, 12}, new int[] {12, 1, 9} );//{1, 9, 12}, {12, 1, 9}
		Assert.assertEquals( expectedValue, actualValue); 

	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = SetEqualArray.isSetEqual( new int[] {9, 1, 12, 1}, new int[] {1, 9, 12, 9, 12, 1, 9} );//{1, 9, 12}, {12, 1, 9}
		Assert.assertEquals( expectedValue, actualValue); //{9, 1, 12, 1}, {1, 9, 12, 9, 12, 1, 9}.

	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 0;
		int actualValue = SetEqualArray.isSetEqual( new int[] {1,7,1}, new int[] {1, 7,6} );
		Assert.assertEquals( expectedValue, actualValue);

	}

}
