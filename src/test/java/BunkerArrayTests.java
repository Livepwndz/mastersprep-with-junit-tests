import org.junit.Assert;
import org.junit.Test;

public class BunkerArrayTests {
	
	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = BunkerArray.isBunkerArray( new int[] {4, 9, 6, 7, 3} );
		Assert.assertTrue(expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 0;
		int actualValue = BunkerArray.isBunkerArray( new int[] {4, 9, 6, 15, 21} );
		Assert.assertTrue(expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = BunkerArray.isBunkerArray( new int[] {4, 8, 6, 16, 22} );
		Assert.assertTrue(expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = BunkerArray.isBunkerArray( new int[] {1,2,3,5,7,11,13} );
		Assert.assertTrue(expectedValue == actualValue );
	}

}
