import org.junit.Assert;
import org.junit.Test;


public class BunkerArrayForOneAndPrime {

	@Test
	public void shouldPass1() {
		
		int expectedValue = 1;
		int actualValue = BunkderArrayForOneAndPrime.isBunker( new int[] {7, 6, 10, 1} );
		Assert.assertTrue( expectedValue == actualValue );

    }
	
	@Test
	public void shouldPass2() {
		
		int expectedValue = 0;
		int actualValue = BunkderArrayForOneAndPrime.isBunker( new int[] {7, 6, 10} );
		Assert.assertTrue( expectedValue == actualValue );

    }
	
	@Test
	public void shouldPass3() {
		
		int expectedValue = 0;
		int actualValue = BunkderArrayForOneAndPrime.isBunker( new int[] {6, 10, 1} );
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );

    }
	
	@Test
	public void shouldPass4() {
		
		int expectedValue = 1;
		int actualValue = BunkderArrayForOneAndPrime.isBunker( new int[] {3, 7, 1, 8, 1} );
		Assert.assertTrue( "Actual value: "+actualValue, expectedValue == actualValue );

    }
	
	@Test public void shouldTestPrime() {
		Assert.assertFalse(Util.isPrime(1));
	}



}
