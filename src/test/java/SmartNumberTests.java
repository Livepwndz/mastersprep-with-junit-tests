import org.junit.Assert;
import org.junit.Test;


public class SmartNumberTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 0;
		int actualValue = SmartNumber.isSmart( 10 );
		Assert.assertEquals( expectedValue, actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = SmartNumber.isSmart( 16 );
		Assert.assertEquals( expectedValue, actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = SmartNumber.isSmart( 22 );
		Assert.assertEquals( expectedValue, actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 0;
		int actualValue = SmartNumber.isSmart( 8 );
		Assert.assertEquals( expectedValue, actualValue );
	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 1;
		int actualValue = SmartNumber.isSmart( 1 );
		Assert.assertEquals( expectedValue, actualValue );
	}


}
