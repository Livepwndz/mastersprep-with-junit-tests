import org.junit.Assert;
import org.junit.Test;



public class MeeraNumberTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = MeeraNumber.isMeera( 6 );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = MeeraNumber.isMeera( 30 );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 0;
		int actualValue = MeeraNumber.isMeera( 21 );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = MeeraNumber.isMeera( 200 );
		Assert.assertTrue( expectedValue == actualValue );
	}

}
