import org.junit.Assert;
import org.junit.Test;

public class NiceArrayTests {

	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = NiceArray.isNice( new int[] {2, 10, 9, 3} );
		Assert.assertTrue( expectedValue == actualValue);

	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = NiceArray.isNice( new int[] {2, 2, 3, 3, 3} );
		Assert.assertTrue( expectedValue == actualValue);

	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = NiceArray.isNice( new int[] {0, -1, 1} );
		Assert.assertTrue( expectedValue == actualValue);

	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 0;
		int actualValue = NiceArray.isNice( new int[] {3, 4, 5, 7} );
		Assert.assertTrue( expectedValue == actualValue);

	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 0;
		int actualValue = NiceArray.isNice( new int[] {3} );
		Assert.assertTrue( expectedValue == actualValue);

	}

}
