import org.junit.Assert;
import org.junit.Test;


public class FactorEqualNumbersTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = FactorEqualNumbers.factorEqual( 10, 33 );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 0;
		int actual = FactorEqualNumbers.factorEqual( 9, 10 );
		Assert.assertEquals(expected, actual);

	}

}
