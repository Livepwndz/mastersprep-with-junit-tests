import org.junit.Assert;
import org.junit.Test;

public class BeanArrayTests {
	
	@Test
	public void shouldPass1() {
		int expectedValue = 1;
		int actualValue = BeanArray.isBean( new int[] {1, 2, 3, 9, 6, 13} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass2() {
		int expectedValue = 1;
		int actualValue = BeanArray.isBean( new int[] {3, 4, 6, 7, 13, 15} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass3() {
		int expectedValue = 1;
		int actualValue = BeanArray.isBean( new int[] {1, 2, 3, 4, 10, 11, 12} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass4() {
		int expectedValue = 1;
		int actualValue = BeanArray.isBean( new int[] {3, 6, 9, 5, 7, 13, 6, 17} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass5() {
		int expectedValue = 0;
		int actualValue = BeanArray.isBean( new int[] {9, 6, 18} );
		Assert.assertTrue( expectedValue == actualValue );
	}
	
	@Test
	public void shouldPass6() {
		int expectedValue = 0;
		int actualValue = BeanArray.isBean( new int[] {4, 7, 16} );
		Assert.assertTrue( expectedValue == actualValue );
	}

}
