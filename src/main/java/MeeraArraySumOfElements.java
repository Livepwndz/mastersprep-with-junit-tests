
public class MeeraArraySumOfElements {

	public static int isMeera(int[] a) {
		
		// Not sure what to return in the case
		if( a == null ) {
			return 0;
		}
		
		int sumOfElements = 0;
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( value >= i) {
				return 0;
			}
			
			sumOfElements = sumOfElements + value;
			
		}
		
		if( sumOfElements != 0 ) {
			return 0;
		}
		
		return 1;
	}

}
