
public class BunkerArray {

	public static int isBunkerArray(int[] a) {
		
		// Not sure exactly what to return in this case.
		if( a == null || a.length == 0) {
			return 0;
		}
		
		
		boolean isOddPresent = false;
		for( int i = 1; i < a.length; i++ ) {
			int value = a[i-1];
			boolean isOdd = value%2 == 1;
			if( isOdd ) {
				isOddPresent = true;
				int nextValue = a[i];
				if( Util.isPrime( nextValue )) {
					return 1;
				}
				
			}
		}
		
		// Not clear if array has no add number it still qualifies a bunke
		// Return 1;
		if( !isOddPresent ) {
			return 1;
		}
		
		
		return 0;
	}

}
