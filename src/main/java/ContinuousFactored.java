
public class ContinuousFactored {

	public static int isContinuousFactored(int n) {
		
		int factorCount = 0;
		for( int i = 2; i < n; i++ ) {
			int value = i;
			int mod = n%value;
			if( mod == 0 ) {
				factorCount++;
			}
			
		}
		
		int[] factors = new int[factorCount];
		int[] products = new int[factorCount];
		for( int i = 2; factorCount > 0; i++ ) {
			int value = i;
			int mod = n%value;
			if( mod == 0 ) {
				factorCount = factorCount - 1;
				factors[ factorCount ] = value;
			}
		}
		
		
		int runningProduct = 1;
		
		for( int i = 0; i < factors.length; i++ ) {
			int factor = factors[i];
			int potentialFactorLeft = factor - 1 ;
			int potentialFactorRight = factor + 1 ;
			boolean isPotentialFactorLeftPresent = false;
			boolean isPotentialFactorRightPresent = false;
			int factorLeft = 1;
			int factorRight = 1;
			
			
			
			for( int j = 0; j < factors.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherFactor = factors[j];
				if( potentialFactorLeft == otherFactor ) {
					isPotentialFactorLeftPresent = true;
					factorLeft = otherFactor;
					break;
				} else if( potentialFactorRight == otherFactor ) {
					isPotentialFactorRightPresent = true;
					factorRight = otherFactor;
					break;
				}
				
			}
			
			int product = factor*factorLeft*factorRight;
			if( product == n ) {
				return 1;
			}
		}
		return 0;
	}

}
