
public class AllPosibilities {

	public static void main(String[] args) {
		int allPosStatus = isAllPosibilities( new int[] {0, 2, 3} );
		System.out.println( "All Posibilities status: "+allPosStatus );

	}

	private static int isAllPosibilities(int[] a) {
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		int n = a.length;
		for( int i = 0; i < n; i++ ) {
			
			boolean isValuePresent = false;
			/*
			int iValue = i;
			
			for( int x = 0; x < n; x++ ) {
				int value = a[x];
				if( iValue == value ) {
					isValuePresent = true;
					break;
				}
			}
			
			if( !isValuePresent ) {
				return 0;
			}*/
			
			
			
			
			int value = a[i];
			if( value < 0 || value >= n  ) {
				return 0;
			}
			
			
			for( int j = 0; j < n; j++ ) {
				if( value == j ) {
					isValuePresent = true;
					break;
				}
			}
			
			if( !isValuePresent ) {
				return 0;
			}
		}
		return 1;
	}

}
