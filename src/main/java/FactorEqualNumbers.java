
public class FactorEqualNumbers {

	public static int factorEqual(int m, int n) {

		int mFactorCount = 0;
		int nFactorCount = 0;

		for (int i = 2; i < m; i++) {
			int value = i;
			boolean isFactor = m % value == 0;
			if (isFactor) {
				mFactorCount++;
			}
		}

		for (int i = 2; i < n; i++) {
			int value = i;
			boolean isFactor = n % value == 0;
			if (isFactor) {
				nFactorCount++;
			}
		}

		if (nFactorCount != mFactorCount) {
			return 0;
		}

		return 1;
	}

}
