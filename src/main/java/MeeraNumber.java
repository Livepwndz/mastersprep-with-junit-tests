
public class MeeraNumber {

	public static int isMeera(int n) {
		if( n <= 5 ) {
			return 0;
		}
		
		int noneTrivialFactorsCount = 0;
		for( int i = 2; i < n; i++ ) {
			int value = i;
			if( n%value == 0 ) {
				noneTrivialFactorsCount++;
			}
		}
		
		if( n%noneTrivialFactorsCount == 0 ) {
			return 1;
		}
		
		
		return 0;
	}

}
