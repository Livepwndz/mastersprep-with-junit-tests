
public class CenteredArrayVersionTwo {

	public static void main(String[] args) {
		int isCenteredStatus = isCentered( new int[] {1} );
		System.out.println( "Is centered: "+isCenteredStatus );

	}

	private static int isCentered(int[] a) {
		if( a == null ) {
			return 0;
		}
		
		int arrayLen = a.length;
		if( arrayLen%2 != 1 ) {
			return 0;
		}
		
		int middleElementIndex = arrayLen/2;
		int middleElementValue = a[ middleElementIndex ];
		
		for( int i = 0; i < arrayLen; i++ ) {
			if( i == middleElementIndex ) {
				continue;
			}
			
			int otherElementValue = a[i];
			if( otherElementValue <= middleElementValue ) {
				return 0;
			}
		}
		
		return 1;
	}

}
