
public class NiceArraySumAndFirstElement {

	public static int isNiceArray(int[] a) {
		
		// Not sure what to return in this case
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		int sumOfPrime = 0;
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( Util.isPrime(value)) {
				sumOfPrime = sumOfPrime + value;
			}
		}
		
		int valueOfFirstIndex = a[0];
		if( valueOfFirstIndex != sumOfPrime ) {
			return 0;
		}
		
		
		return 1;
	}
	
	

}
