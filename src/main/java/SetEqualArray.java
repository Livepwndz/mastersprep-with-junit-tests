
public class SetEqualArray {

	public static int isSetEqual(int[] a, int[] b) {
		// Not sure what to return here
		if (a == null || b == null) {
			return 0;
		}

		if (a.length == 0 && b.length == 0) {
			return 1;
		}

		for (int i = 0; i < a.length; i++) {
			int aValue = a[i];
			boolean isAValuePresent = false;
			for (int j = 0; j < b.length; j++) {
				int bValue = b[j];
				if (aValue == bValue) {
					isAValuePresent = true;
					break;
				}
			}

			if (!isAValuePresent) {
				return 0;
			}
		}
		
		for (int i = 0; i < b.length; i++) {
			int bValue = b[i];
			boolean isBValuePresent = false;
			for (int j = 0; j < a.length; j++) {
				int aValue = a[j];
				if (aValue == bValue) {
					isBValuePresent = true;
					break;
				}
			}

			if (!isBValuePresent) {
				return 0;
			}
		}

		return 1;
	}

}
