
public class CompleteArrayMaxEvenAndAllEvenNumber {

	public static int isComplete(int[] a) {

		// Not so sure what to return in this case
		if (a == null || a.length == 0) {
			return 0;
		}

		int maxEvenValue = 0;
		int evenValueCount = 0;

		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			boolean isEven = value % 2 == 0;
			if (isEven) {
				if (value > maxEvenValue) {
					maxEvenValue = value;
				}
				evenValueCount++;
			}
		}

		int[] evens = new int[evenValueCount];

		for (int i = 0; evenValueCount > 0; i++) {
			int value = a[i];
			boolean isEven = value % 2 == 0;
			if (isEven) {
				evenValueCount = evenValueCount - 1;
				evens[evenValueCount] = value;
			}
		}

		for (int i = 2; i < maxEvenValue;) {

			int even = i;
			boolean isEvenPresent = false;
			for (int j = 0; j < evens.length; j++) {
				int evenValueInArray = evens[j];
				if (evenValueInArray == even) {
					isEvenPresent = true;
					break;
				}
			}

			if (!isEvenPresent) {
				return 0;
			}

			i = i + 2;

		}

		return 1;
	}

}
