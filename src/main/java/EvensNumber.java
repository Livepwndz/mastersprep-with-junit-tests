
public class EvensNumber {

	public static void main(String[] args) {
		int isEvensStatus = isEvens( 2182);
		System.out.println( "Is evens: "+isEvensStatus ); 
	}

	private static int isEvens(int  n) {
		while( n%10 != 0 ) {
			int mod = n%10;
			if( mod%2 != 0 ) {
				return 0;
			}
			
			n = n/10;
		}
		return 1;
	}

}
