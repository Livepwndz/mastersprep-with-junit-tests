
public class TwinArray {

	public static int isTwin(int[] a) {
		// Not sure what to return here
		if( a == null ) {
			return 0;
		}
		
		int primeCount = 0;
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( Util.isPrime( value )) {
				primeCount++;
			}
		}
		
		int[] primes = new int[primeCount];
		
		for( int i = 0; primeCount > 0; i++ ) {
			int value = a[i];
			if( Util.isPrime( value )) {
				primeCount = primeCount - 1;
				primes[primeCount] = value;
			}
		}
		
		for( int i = 0; i < primes.length; i++ ) {
			int prime = primes[i];
			int potentialTwinLeft = prime-2;
			int potentialTwinRight = prime+2;
			if( !Util.isPrime(potentialTwinLeft) && !Util.isPrime(potentialTwinRight)) {
				continue;
			}
			
			
			boolean isTwinPresent = false;
			for( int j = 0; j < primes.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherPrime = primes[j];
				int diff = Math.abs( prime - otherPrime );
				if( diff == 2 ) {
					isTwinPresent = true;
					break;
				}
			}
			
			if( !isTwinPresent ) {
				return 0;
			}
		}
		
		return 1;
	}

}
