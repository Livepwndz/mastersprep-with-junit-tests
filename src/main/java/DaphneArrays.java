
public class DaphneArrays {
	
	public static void main( String[] args ) {
		int daphneStatus = isDaphne( new int[]{3, 2, 5} );
		System.out.println( "Daphne status: "+daphneStatus );
	}

	private static Integer isDaphne(int[] a) {
		if( a == null || a.length == 0 )
			return null;
		
		
		
		int mod = Math.abs( a[0]%2 );
		
		for( int i = 1; i < a.length; i++ ) {
			int valueMod = Math.abs(a[i]%2);
			if( valueMod != mod ) {
				return 0;
			}
		}
		
		return 1;
	}

}
