
public class FibonacciNumber {

	public static int isFibonacci(int n) {
		if( n < 1 ) {
			return 0;
		}
		
		if( n == 1 ) {
			return 1;
		}
		
		int firstFib = 1;
		int secondFib = 1;
		
		
		
		for( int i = 2; i <= n;) {
			int nextFib = firstFib + secondFib;
			if( nextFib == n ) {
				return 1;
			}
			
			firstFib = secondFib;
			secondFib = nextFib;
			i = nextFib+1;
			
		}
		
		
		return 0;
	}

}
