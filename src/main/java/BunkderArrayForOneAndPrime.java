
public class BunkderArrayForOneAndPrime {

	public static int isBunker(int[] a) {

		// Not sure what to return in this case;
		if (a == null) {
			return 0;
		}

		// What happens if array contains one element

		boolean isOnePresent = false;
		boolean isPrimePresent = false;
		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			if (!isOnePresent && value == 1) {
				isOnePresent = true;
			}

			if (!isPrimePresent && Util.isPrime(value)) {
				isPrimePresent = true;
			}
		}

		// I don't know what to return if neither 1 or prime are present.
		if( !isPrimePresent && !isOnePresent ) {
			return 1;
		}
		
		if( isPrimePresent && isOnePresent ) {
			return 1;
		}
		
		return 0;
		
		
	}

}
