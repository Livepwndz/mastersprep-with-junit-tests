
public class TripleArray {

	public static int isTriple(int[] a ) {
		
		if( a == null || a.length < 3 ) {
			return 0;
		}
		
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			int valueCount = 1;
			for( int j = 0; j < a.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherValue = a[j];
				if( value == otherValue) {
					valueCount = valueCount + 1;
				}
			}
			
			if( valueCount != 3 ) {
				return 0;
			}
		}
		
		return 1;
	}

}
