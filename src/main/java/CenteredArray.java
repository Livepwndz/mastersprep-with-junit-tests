
public class CenteredArray {

	public static void main(String[] args) {

		int[] a = new int[] {10};

		if (a.length % 2 == 0) {

			System.out.println("Return 0");

		} else {

			int length = a.length;
			int middleElementIndex = (length / 2);
			

			boolean isMiddleElementLessThanValues = true;

			int middleElementValue = a[middleElementIndex];
			
			System.out.println("Middle element index: "+middleElementIndex);
			System.out.println("Middle element value: "+middleElementValue);
			for (int i = 0; i < length; i++) {
				if (i != middleElementIndex) {

					int value = a[i];

					if (value > middleElementValue) {
						isMiddleElementLessThanValues = true;
					} else {
						isMiddleElementLessThanValues = false;
						break;

					}
				}

			}

			if (isMiddleElementLessThanValues) {
				System.out.println("Return 1");
			} else {
				System.out.println("Return 0");
			}

		}

	}

}
