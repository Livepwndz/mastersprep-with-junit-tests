
public class BalancedArray {

	public static void main(String[] args) {
		int isBalancedStatus = isBalanced(new int[] { -5, 2, -2 });
		System.out.println("Is balanced status: " + isBalancedStatus);

	}

	private static int isBalanced(int[] a) {

		// I didn't know what to return if a is null;
		if (a == null) {
			return 0;
		}

		if (a.length == 0) {
			return 1;
		}

		for (int i = 0; i < a.length; i++) {
			int value = a[i];

			boolean isValueOfDifferentParity = false;
			for (int j = 0; j < a.length; j++) {
				if (i == j) {
					continue;
				}

				int otherValue = a[j];
				if (Math.negateExact(value) == otherValue) {
					isValueOfDifferentParity = true;
					break;
				}

			}

			if (!isValueOfDifferentParity) {
				return 0;
			}
		}

		return 1;
	}

}
