
public class WaveArray {

	public static int isWave(int[] a) {
		/*
		 * I am not sure of what to return if a is null or empty
		 * returning 0 for now.
		 */

		if (a == null || a.length == 0) {
			return 0;
		}
		
		

		for (int i = 1; i < a.length; i++) {
			int value = a[i-1];
			int nextValue = a[i];

			int modOfValue = value % 2;
			int modOfNextValue = nextValue % 2;
			if (modOfValue == modOfNextValue) {
				return 0;
			}
		}

		return 1;
	}

}
