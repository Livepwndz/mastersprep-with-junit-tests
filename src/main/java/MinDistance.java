
public class MinDistance {

	public static int findFor(int n) {
		
		int factorCount = 0;
		for( int i = 1; i <= n; i++ ) {
			int mod = n%i;
			if( mod == 0 ) {
				factorCount++;
			}
		}
		
		int[] factors = new int[ factorCount ];
		
		for( int i = 1; factorCount > 0; i++ ) {
			int value = i;
			int mod = n%value;
			if( mod == 0 ) {
				factorCount = factorCount - 1;
				factors[factorCount] = value;
			}
		}
		
		
		
		
		int minFactorDiff = n - 1;
		for( int i = 0; i < factors.length; i++ ) {
			int value = factors[i];
			for( int j = 0; j < factors.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherValue = factors[j];
				int factorDiff = Math.abs( value-otherValue );
				if( factorDiff < minFactorDiff ) {
					minFactorDiff = factorDiff;
				}
				
			}
		}
		
		return minFactorDiff;
	}

}
