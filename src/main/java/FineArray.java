
public class FineArray {

	public static void main(String[] args) {
		int[] a = new int[] { 4, 6 };
		int isFineStatus = isFine(a);
		System.out.println("Is Fine status: " + isFineStatus);

		isPrime(a);

	}

	private static void isPrime(int[] a) {
		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			boolean isPrimeStatus = isPrime(value);
			System.out.println("Is Prime status of " + value + ": " + isPrimeStatus);
		}

	}

	private static int isFine(int[] a) {
		if (a == null || a.length == 0) {
			return 1;
		}

		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			if (isPrime(value)) {
				boolean isTwinPresent = false;
				for (int j = 0; j < a.length; j++) {
					if (i == j)
						continue;

					int otherValue = a[j];
					if (isPrime(otherValue)) {
						int diff = Math.abs( value - otherValue );
						if ( diff == 2 ) {
							isTwinPresent = true;
							break;
						}
					}

				}

				if (!isTwinPresent) {
					return 0;
				}
			}
		}


		return 1;
	}

	private static boolean isPrime(int value) {
		if( value < 0 ) {
			return false;
		}
		for (int i = 2; i < value; i++) {
			int mod = value % i;
			if (mod == 0) {
				return false;
			}
		}
		return true;
	}

}
