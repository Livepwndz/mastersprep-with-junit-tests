
public class MeeraArray {

	public static int isMeera(int[] a ) {
		// Not so sure what to return in this case
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		
		
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			int valueProduct = 2*value;
			
			for( int j = 0; j < a.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherValue = a[j];
				if( otherValue == valueProduct ) {
					return 0;
				}
			}
		}
		
		return 1;
	}

}
