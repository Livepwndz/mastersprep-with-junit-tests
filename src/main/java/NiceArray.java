
public class NiceArray {

	public static int isNice(int[] a) {
		// Not sure what to return in this case
		if (a == null) {
			return 0;
		}

		if (a.length == 1) {
			return 0;
		}

		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			boolean isValueNice = false;
			for (int j = 0; j < a.length; j++) {
				if (i == j) {
					continue;
				}

				int otherValue = a[j];
				boolean isNice = Math.abs(value - otherValue) == 1;

				if (isNice) {
					isValueNice = true;
					break;
				}
			}

			if (!isValueNice) {
				return 0;
			}
		}

		return 1;
	}

}
