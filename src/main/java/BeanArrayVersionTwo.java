
public class BeanArrayVersionTwo {

	public static int isBean(int[] a) {
		
		// Not sure what to return in this instance
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		if( a.length == 1 ) {
			return 0;
		}
		
		for( int i = 2; i < a.length; i++ ) {
			int value = a[i];
			boolean isBeanPresent = false;
			for( int j = 0; j < a.length; j++ ) {
				if( i == j ) {
					continue;
				}
				
				int otherValue = a[j];
				int diff = Math.abs( value - otherValue );
				if( diff == 1 ) {
					isBeanPresent = true;
					break;
				}
				
			}
			
			if( !isBeanPresent ) {
				return 0;
			}
		}
		return 1;
	}

}
