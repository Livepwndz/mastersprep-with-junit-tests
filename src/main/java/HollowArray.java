
public class HollowArray {

	static int isHallow(int[] a) {
		if (a == null || a.length < 6) {
			return 0;
		}

		int arrayLen = a.length;
		int mod = arrayLen % 2;
		if (mod == 0) {
			return 0;
		}

		int middleIndex = arrayLen / 2;
		int middleIndexValue = a[middleIndex];
		if (middleIndexValue != 0) {
			return 0;
		}

		int onLeftZeroCount = 0;
		for (int i = middleIndex - 1; i >= 0; i--) {
			int value = a[i];
			if (value != 0) {
				break;
			}
			onLeftZeroCount++;
		}

		int onRightZeroCount = 0;
		int indexOfFirstNoneZeroOnRight = 0;
		for (int i = middleIndex + 1; i < a.length; i++) {
			int value = a[i];
			if (value != 0) {
				indexOfFirstNoneZeroOnRight = i;
				break;
			}
			onRightZeroCount++;
		}
		
		

		if (onLeftZeroCount != onRightZeroCount) {
			return 0;
		}

		int countOfZerosInTheMiddle = onLeftZeroCount + onRightZeroCount + 1;
		if (countOfZerosInTheMiddle < 3) {
			return 0;
		}

		int countOfNoneZerosToTheRight = 1;

		for (int i = indexOfFirstNoneZeroOnRight + 1; i < a.length; i++) {
			int value = a[i];
			if (value == 0 && (countOfZerosInTheMiddle != countOfNoneZerosToTheRight)) {
				return 0;
			}
			countOfNoneZerosToTheRight = countOfNoneZerosToTheRight + 1;
		}

		if (countOfZerosInTheMiddle != countOfNoneZerosToTheRight) {
			return 0;
		}

		return 1;
	}

}
