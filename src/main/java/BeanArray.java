
public class BeanArray {

	public static int isBean(int[] a) {
		
		/*
		 * I am not sure of what to return if a is null
		 * returning 0 for now.
		 */
		
		if( a == null ) {
			return 0;
		}
		
		boolean is9Present = false;
		boolean is13Present = false;
		boolean is7Present = false;
		boolean is16Present = false;
		
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( value == 9 ) {
				is9Present = true;
			} else if( value == 13 ) {
				is13Present = true;
			} else if( value == 7 ) {
				is7Present = true;
			} else if( value == 16 ) {
				is16Present = true;
			}
		}
		
		
		if( is9Present && !is13Present ) {
			return 0;
		}
		
		
		if( is7Present && is16Present ) {
			return 0;
		}
		
		
		
		return 1;
	}

}
