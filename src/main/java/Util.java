
public class Util {
	public static boolean isPrime(int value) {
		if( value <= 1 ) {
			return false;
		}
		for (int i = 2; i < value; i++) {
			int mod = value % i;
			if (mod == 0) {
				return false;
			}
		}
		
		// System.out.println( "Is "+value+" prime?: "+ true); 
		return true;
	}

}
