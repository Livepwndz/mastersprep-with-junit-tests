import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

public class FileCleaner {
	private static final String HOME = System.getProperty("user.home");

	public static void main(String[] args) throws URISyntaxException, IOException {

		new FileCleaner().cleanFile();
	}

	private void cleanFile() throws URISyntaxException, IOException {
		URL sourceDir = FileCleaner.class.getClassLoader().getResource("migration/data/dev/dirty");
		try (Stream<Path> stream = Files.walk(Paths.get(sourceDir.toURI()))) {
			stream.filter(Files::isRegularFile).filter(path -> !path.startsWith("clean"))
					.forEach(FileCleaner::cleanFile);
		}

	}

	private static void cleanFile(Path path) {

		try {

			Path pathFileName = path.getFileName();

			System.out.println("Migration Path file name: " + pathFileName);
			Path newPath = Files.createTempFile("gh_", "json");
			BufferedWriter writer = Files.newBufferedWriter(newPath);

			StringBuilder linesBuilder = new StringBuilder();

			Files.lines(path).forEach(line -> {
				linesBuilder.append(line);
				linesBuilder.append("\n");
			});

			cleanLines(writer, linesBuilder.toString());
			writer.close();

			String movePathFileName = "clean_gh_dev_" + pathFileName;
			String movePathStr = HOME + "/Desktop/gh-migration/" + movePathFileName;
			Path movePath = Paths.get(movePathStr);
			Files.move(newPath, movePath, StandardCopyOption.REPLACE_EXISTING);

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new RuntimeException("Error clean migration file.");
		}

	}

	private static void cleanLines(BufferedWriter writer, String line) {

		String newLine = line.replace("ObjectId(", "").replace(")", "").replace("ISODate(", "")
				.replace("NumberInt(", "").replace("+0000", "").replace("}\n{", "},\n{");

		try {
			writer.append("[");
			writer.append(newLine);
			writer.append("]");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error occured while writing.");
		}

	}

}
