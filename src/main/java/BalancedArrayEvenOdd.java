
public class BalancedArrayEvenOdd {

	public static void main(String[] args) {
		int isBalancedStatus = isBalanced(new int[] {16, 6, 2, 3});
		System.out.println("Is balanced status: " + isBalancedStatus);

	}

	private static int isBalanced(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}

		for (int i = 0; i < a.length; i++) {
			int value = a[i];
			boolean isIndexEven = i % 2 == 0;
			boolean isValueEven = value % 2 == 0;
			if (isIndexEven) {
				if (!isValueEven) {
					return 0;
				}
			} else {
				if (isValueEven) {
					return 0;
				}
			}
		}
		return 1;
	}

}
