
public class DigitCountInNumber {

	public static int countDigit(int value, int digit) {

		if (value < 0 || digit < 0) {
			return -1;
		}

		int digitCount = 0;
		do  {
			
			int mod = value % 10;
			if (mod == digit) {
				digitCount++;
			}

			value = value / 10;
		} while (value % 10 != 0);

		return digitCount;
	}

}
