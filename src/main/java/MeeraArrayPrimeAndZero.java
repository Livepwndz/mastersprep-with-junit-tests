
public class MeeraArrayPrimeAndZero {

	public static int isMeera(int[] a ) {
		
		boolean isZeroPresent = false;
		boolean isPrimePresent = false;
		
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( !isZeroPresent && value == 0  ) {
				isZeroPresent = true;
			}
			
			if( !isPrimePresent && Util.isPrime(value)) {
				isPrimePresent = true;
			}
			
			if( isPrimePresent && isZeroPresent ) {
				return 1;
			}
			
		}
		
		if( !isPrimePresent && !isZeroPresent ) {
			return 1;
		}
		
		return 0;
	}

}
