import java.util.Arrays;
import java.util.stream.Collectors;

public class FillArray {

	public static void main(String[] args) {
		int k = 2;
		int n = 4;
		Integer[] fillArr = fillArray(new int[]{2, 6, 9, 0, -3}, 0, 4);
		if (fillArr == null) {
			System.out.println("Fill array: NULL");
			return;
		}

		String stringOfElements = Arrays.asList(fillArr).stream().map(value -> value.toString())
				.collect(Collectors.joining(","));
		System.out.println("Fill array: " + stringOfElements);

	}

	private static Integer[] fillArray(int[] arr, int k, int n) {
		if (k <= 0 || n <= 0) {
			return null;
		}

		int kIndex = 0;
		Integer[] arr2 = new Integer[n];

		for (int i = 0; i < n; i++) {
			int value = arr[kIndex];
			arr2[i] = value;

			if (kIndex == k - 1) {
				kIndex = 0;
			} else {
				kIndex = kIndex + 1;
			}
		}

		return arr2;
	}

}
